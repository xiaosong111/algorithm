package com.qg.array;

import com.qg.link.DoublyLinkedListSentinel;
import com.qg.link.DoublyLoopLinkedListSentinel;
import org.junit.jupiter.api.Test;

/**
 * @Author QG
 * @Date 2023/5/30 9:30
 * @description
 **/
public class DoublyLinkedListTest {

    @Test
    public void testDoubleList() {
        DoublyLinkedListSentinel<Integer> list = new DoublyLinkedListSentinel<>();
        list.insert(0,0);
//        list.insert(1,1);
//        list.insert(2,2);
        list.insert(1,5);
        list.addLast(1);
        list.removeLast();
        list.addLast(2);
        System.out.println(list);
        for (Integer integer : list) {
            System.out.println(integer);
        }
        System.out.println("length ==> " + list.length());
    }

    @Test
    public void testDoubleListLoop() {
        DoublyLoopLinkedListSentinel<Integer> list = new DoublyLoopLinkedListSentinel<>();
        list.addLast(9);
        list.addLast(1);
        list.addLast(1);
        list.addFirst(0);
        list.addFirst(0);
        list.addFirst(0);
        list.addLast(1);

        list.removeLast();
        list.removeByValue(9);

        for (Integer integer : list) {
            System.out.println(integer);
        }
        System.out.println("length ==> " + list.length());
    }

}
