package com.qg.array;

import org.junit.jupiter.api.Test;

/**
 * @Author QG
 * @Date 2023/5/20 10:46
 * @description
 **/

public class DynamicArrayTest {

    @Test
    public void arrayTest() {
        DynamicArray dynamicArray = new DynamicArray();
        dynamicArray.addLast(1);
        dynamicArray.addLast(2);
        dynamicArray.addLast(3);
        dynamicArray.addLast(6);
        dynamicArray.addLast(4);
        dynamicArray.addLast(5);
        //测试get()方法
//        for (int i = 0; i < 5; i++) {
//            System.out.println(dynamicArray.get(i));
//
//        }
//        测试迭代器
        for (Integer integer : dynamicArray) {
            System.out.println(integer);
        }
        System.out.println(dynamicArray);

        dynamicArray.remove(2);
        dynamicArray.remove(3);
        System.out.println("++++++++++++++");
        //测试foreach方法
        dynamicArray.foreach(i -> {
            System.out.println("success accept data is ==> " + i);
        });
        System.out.println(dynamicArray.stream().count());
    }
}
