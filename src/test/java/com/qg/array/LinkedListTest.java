package com.qg.array;

import com.qg.link.SinglyLinkedList;
import com.qg.link.SinglyLinkedListSentinel;
import org.junit.jupiter.api.Test;

/**
 * @Author QG
 * @Date 2023/5/23 22:18
 * @description
 **/
public class LinkedListTest {

    @Test
    public void testLink() {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();
        list.addLast(1);
        list.addLast(2);
        list.addLast(3);
        list.addLast(4);
        list.insert(0,0);

        list.loop3(System.out::println);

//        System.out.println(list.get(2));
//        System.out.println(list.get(0));
//        System.out.println(list.get(3));

//        list.loop(System.out::print);
//        System.out.println("===========");
//        list.loop2(System.out::println);
//        System.out.println("===========");
//        for (Integer integer : list) {
//            System.out.print(integer + " ");
//        }

    }

    @Test
    public void testLink2() {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();
        list.addLast(1);
        list.addLast(2);
        list.addLast(3);
        list.addLast(4);

        list.insert(0,0);
        list.loop(System.out::print);

        list.removeFirst();
        list.remove(0);

        System.out.println();
        for (Integer integer : list) {
            System.out.println(integer);
        }
        System.out.println("list size is " + list.length());

    }

    @Test
    public void testLink3() {
        SinglyLinkedListSentinel<Integer> list = new SinglyLinkedListSentinel<>();
        list.addLast(1);
        list.addLast(2);
        list.addLast(3);
        list.addLast(4);

        list.insert(0,0);

        list.removeFirst();
        list.loop2(System.out::print);
        list.remove(0);

        System.out.println();
        for (Integer integer : list) {
            System.out.println(integer);
        }
        System.out.println("list size is " + list.length());

    }
}
