package com.qg.recursion;

import java.util.LinkedList;

/**
 * @Author QG
 * @Date 2023/5/31 9:18
 * @description 递归相关
 **/
public class RecursionTest {

    /**
     * 递归求阶乘
     *
     * @param n
     * @return
     */
    public int getFactorial(int n) {
        if (n == 1) return 1;
        return n * getFactorial(n - 1);
    }

    /**
     * 递归反向打印字符串
     *
     * @param s
     */
    public static void reversePrintString(String s) {
        recursionPrintString(s, 0);
    }

    public static void recursionPrintString(String s, int i) {
        if (i == s.length()) {
            return;
        }
        recursionPrintString(s, i + 1);
        System.out.println(s.charAt(i));
    }

    /**
     * 递归二分查找
     */
    public static void binarySearch(int[] nums, int target) {
        int l = 0, r = nums.length - 1;
        int i = recursionBinary(nums, target, l, r);
        System.out.println(i);
    }

    private static int recursionBinary(int[] nums, int target, int l, int r) {
        if (l > r) {
            return -1;
        }
        int mid = (l + r) >>> 1;
        if (nums[mid] > target) {
            return recursionBinary(nums, target, l, mid - 1);
        } else if (nums[mid] < target) {
            return recursionBinary(nums, target, mid + 1, r);
        } else {
            return mid;
        }
    }

    /**
     * 递归冒泡排序
     *
     * @param nums
     * @param j
     */
    public static void bubbleSort(int[] nums, int j) {
        if (j == 0) {
            return;
        }
        int x = 0;
        for (int i = 0; i < j; i++) {
            if (nums[i] > nums[i + 1]) {
                int t = nums[i];
                nums[i] = nums[i + 1];
                nums[i + 1] = t;
                x = i;
            }
        }
        bubbleSort(nums, x);
    }

    /**
     * 递归插入排序
     *
     * @param nums
     * @param low
     */
    public static void insertionSort(int[] nums, int low) {
        if (low == nums.length) {
            return;
        }
        int t = nums[low];
        int i = low - 1;
        while (i >= 0 && nums[i] > t) {
            nums[i + 1] = nums[i];
            i--;
        }
        //找到了插入位置
        nums[i + 1] = t;
        insertionSort(nums, low + 1);
    }

    /**
     * 汉洛塔
     */
    public static void hanlota(int n) {
        LinkedList<Integer> a = new LinkedList<>();
        LinkedList<Integer> b = new LinkedList<>();
        LinkedList<Integer> c = new LinkedList<>();
        for (int i = 1; i <= n; i++) {
            a.addFirst(i);
        }
        move(n, a, b, c);
        System.out.println(c);
    }

    public static void move(int n, LinkedList<Integer> a, LinkedList<Integer> b, LinkedList<Integer> c) {
        if (n == 0) return;
        move(n - 1, a, c, b);
        c.addLast(a.removeLast());
        move(n - 1, b, a, c);
    }

    /**
     * 杨辉三角
     *
     * @param n
     */
    public static void triangleByYH(int n) {
        for (int i = 0; i < n; i++) {
            String s = new String();
            for (int j = 0; j < (n - 1 - i); j++) {
                s += "  ";
            }
            System.out.print(s);
            for (int j = 0; j <= i; j++) {
                System.out.printf("%-4s", element(i, j));
            }
            System.out.println();
        }
    }

    private static int element(int i, int j) {
        if (j == 0 || i == j) {
            return 1;
        }
        return element(i - 1, j - 1) + element(i - 1, j);
    }


    /**
     * 杨辉三角优化
     *
     * @param n
     */
    public static void triangleByYH2(int n) {
        int[][] triangle = new int[n][];
        for (int i = 0; i < n; i++) {
            String s = new String();
            for (int j = 0; j < (n - 1 - i); j++) {
                s += "  ";
            }
            System.out.print(s);
            triangle[i] = new int[i + 1];
            for (int j = 0; j <= i; j++) {
                System.out.printf("%-4s", element2(triangle, i, j));
            }
            System.out.println();
        }
    }


    private static int element2(int[][] triangle, int i, int j) {
        if (triangle[i][j] > 0) {
            return triangle[i][j];
        }
        if (j == 0 || i == j) {
            triangle[i][j] = 1;
            return 1;
        }

        triangle[i][j] = element2(triangle, i - 1, j - 1) + element2(triangle, i - 1, j);
        return triangle[i][j];
    }


    /**
     * 根据上一行生成杨辉三角行
     *
     * @param rows
     * @param i
     */
    public static void createRow(int[] rows, int i) {
        if (i == 0) {
            rows[0] = 1;
        }
        for (int j = i; j > 0; j--) {
            rows[j] = rows[j] + rows[j - 1];
        }
    }

    /**
     * 杨辉三角优化
     *
     * @param n
     */
    public static void triangleByYH3(int n) {
        int[] row = new int[n];
        for (int i = 0; i < n; i++) {
            String s = new String();
            for (int j = 0; j < (n - 1 - i); j++) {
                s += "  ";
            }
            System.out.print(s);
            createRow(row,i);
            for (int j = 0; j <= i; j++) {
                System.out.printf("%-4s",row[j] );
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        triangleByYH3(6);
    }


}
