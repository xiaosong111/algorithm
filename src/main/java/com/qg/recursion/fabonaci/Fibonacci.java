package com.qg.recursion.fabonaci;

import java.util.Arrays;

/**
 * @Author QG
 * @Date 2023/6/2 9:51
 * @description
 **/
public class Fibonacci {

    /**
     * 斐波那契数列
     *
     * @param n
     * @return
     */
    public int fibonacci(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("非法参数");
        } else if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    /**
     * 斐波那契优化（剪枝） ，使用记忆法
     *
     * @param n
     * @return
     */
    public int fibonacciByPruning(int n) {
        int[] cache = new int[n + 2];
        Arrays.fill(cache, -1);
        cache[0] = 0;
        cache[1] = -1;
        return f(n,cache);
    }

    private int f(int n, int[] cache) {
        if(cache[n] == -1) {
            int x = f(n-1,cache);
            int y = f(n-2,cache);
            cache[n] = x + y;
        }
        return cache[n];
    }
}
