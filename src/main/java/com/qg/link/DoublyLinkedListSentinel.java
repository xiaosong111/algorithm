package com.qg.link;

import java.util.Iterator;

/**
 * @Author QG
 * @Date 2023/5/30 9:20
 * @description 带哨兵的双向链表
 **/
public class DoublyLinkedListSentinel<T> implements Iterable<T> {

    /**
     * 头哨兵
     */
    private Node<T> head;
    /**
     * 尾哨兵
     */
    private Node<T> tail;

    public DoublyLinkedListSentinel() {
        this.head = new Node<T>(null, null, null);
        this.tail = new Node<T>(null, null, null);
        head.next = tail;
        tail.prev = head;
    }

    private Node<T> findNode(int index) {
        int i = -1;
        for (Node<T> p = head; p != tail; p = p.next, i++) {
            if (i == index) {
                return p;
            }
        }
        return null;
    }

    /**
     * 插入新元素
     *
     * @param index
     * @param value
     */
    public void insert(int index, T value) {
        Node<T> prev = findNode(index - 1);
        if (prev == null) {
            throw getIllegalIndex(index);
        }
        Node<T> next = prev.next;
        Node<T> insert = new Node<>(prev, value, next);
        prev.next = insert;
        next.prev = insert;
    }

    /**
     * 删除指定索引处的元素
     * @param index
     */
    public void remove(int index) {
        Node<T> prev = findNode(index - 1);
        if(prev == null) {
            throw getIllegalIndex(index);
        }
        Node<T> removed = prev.next;
        if(removed == tail) {
            throw getIllegalIndex(index);
        }
        Node<T> next = removed.next;
        prev.next = next;
        next.prev = prev;
    }

    /**
     * 链表尾部删除元素
     */
    public void removeLast() {
        Node<T> removed = tail.prev;
        if(removed == head) {
            throw getIllegalIndex(0);
        }
        Node<T> prev = removed.prev;
        prev.next = tail;
        tail.prev = prev;
    }

    /**
     * 链表尾部添加元素
     * @param value
     */
    public void addLast(T value) {
        Node<T> prev = tail.prev;
        Node<T> node = new Node<>(prev, value, tail);
        prev.next = node;
        tail.prev = node;
    }

    /**
     * 链表头部插入元素
     * @param value
     */
    public void addFirst(T value) {
        Node<T> next = head.next;
        Node<T> node = new Node<>(head, value, next);
        head.next = node;
        next.prev = node;
    }


    /**
     * 移除链表头部元素
     */
    public void removeFirst() {
        Node<T> removed = head.next;
        if(removed == tail) {
            throw getIllegalIndex(0);
        }
        Node<T> next = removed.next;
        head.next = next;
        next.prev = head;
    }

    private IllegalArgumentException getIllegalIndex(int index) {
        return new IllegalArgumentException(String.format("index [%d] 不合法", index));
    }

    public int length() {
        Node<T> p = head.next;
        int i = 0;
        while (p != tail) {
            i++;
            p = p.next;
        }
        return i;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> p = head.next;

            @Override
            public boolean hasNext() {
                return p != tail;
            }

            @Override
            public T next() {
                T value = p.value;
                p = p.next;
                return value;
            }
        };
    }

    static class Node<T> {
        Node<T> prev;
        T value;
        Node<T> next;

        public Node(Node<T> prev, T value, Node<T> next) {
            this.prev = prev;
            this.value = value;
            this.next = next;
        }
    }

}
