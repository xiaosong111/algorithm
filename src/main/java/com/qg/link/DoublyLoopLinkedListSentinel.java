package com.qg.link;

import java.util.Iterator;

/**
 * @Author QG
 * @Date 2023/5/30 10:43
 * @description 带哨兵的双向循环链表
 **/
public class DoublyLoopLinkedListSentinel<T> implements Iterable<T> {

    private Node<T> sentinel = new Node<>(null, null, null);

    public DoublyLoopLinkedListSentinel() {
        sentinel.prev = sentinel;
        sentinel.next = sentinel;
    }


    /**
     * 链表头部插入元素
     *
     * @param value
     */
    public void addFirst(T value) {
        Node<T> next = sentinel.next;
        Node<T> added = new Node<>(sentinel, value, next);
        sentinel.next = added;
        next.prev = added;
    }

    /**
     * 链表尾部插入元素
     *
     * @param value
     */
    public void addLast(T value) {
        Node<T> prev = sentinel.prev;
        Node<T> added = new Node<>(prev, value, sentinel);
        prev.next = added;
        sentinel.prev = added;
    }

    /**
     * 删除第一个节点
     */
    public void removeFirst() {
        Node<T> removed = sentinel.next;
        if (removed == sentinel) {
            throw new IllegalArgumentException("非法");
        }
        Node<T> next = removed.next;
        sentinel.next = next;
        next.prev = sentinel;
    }

    /**
     * 删除最后一个节点
     */
    public void removeLast() {
        Node<T> removed = sentinel.prev;
        if (removed == sentinel) {
            throw new IllegalArgumentException("非法");
        }
        Node<T> prev = removed.prev;
        prev.next = sentinel;
        sentinel.prev = prev;
    }


    /**
     * 删除特定元素值的节点，如果有值相等的节点，删除第一个
     *
     * @param value
     */
    public void removeByValue(T value) {
        Node<T> removed = findByValue(value);
        if (removed == null) {
            throw new IllegalArgumentException("非法");
        }
        Node<T> prev = removed.prev;
        Node<T> next = removed.next;
        prev.next = next;
        next.prev = prev;
    }

    public Node<T> findByValue(T value) {
        Node<T> p = sentinel.next;
        while (p != sentinel) {
            if (value.equals(p.value)) {
                return p;
            }
            p = p.next;
        }
        return null;
    }

    /**
     * 获取链表长度
     *
     * @return
     */
    public int length() {
        Node<T> p = sentinel.next;
        int i = 0;
        while (p != sentinel) {
            i++;
            p = p.next;
        }
        return i;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            Node<T> p = sentinel.next;

            @Override
            public boolean hasNext() {
                return p != sentinel;
            }

            @Override
            public T next() {
                T value = p.value;
                ;
                p = p.next;
                return value;
            }
        };
    }

    private static class Node<T> {
        Node<T> prev;
        T value;
        Node<T> next;

        public Node(Node<T> prev, T value, Node<T> next) {
            this.prev = prev;
            this.value = value;
            this.next = next;
        }
    }
}
