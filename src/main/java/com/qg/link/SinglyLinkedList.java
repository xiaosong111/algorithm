package com.qg.link;

import java.util.Iterator;
import java.util.function.Consumer;

/**
 * @Author QG
 * @Date 2023/5/23 21:53
 * @description 单向链表
 **/
public class SinglyLinkedList<T> implements Iterable<T> {

    /**
     * 头节点
     */
    private Node<T> head;


    private static class Node<T> {
        private T value;
        private Node<T> next;

        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }
    }

    /**
     * 在链表头部增加节点
     *
     * @param value
     */
    public void addFirst(T value) {
        head = new Node<T>(value, head);
    }


    /**
     * 循环遍历while循环实现
     *
     * @param consumer
     */
    public void loop(Consumer<T> consumer) {
        Node<T> p = head;
        while (p != null) {
            consumer.accept(p.value);
            p = p.next;
        }
    }

    /**
     * 循环遍历for循环实现
     *
     * @param consumer
     */
    public void loop2(Consumer<T> consumer) {
        for (Node<T> p = head; p != null; p = p.next) {
            consumer.accept(p.value);
        }
    }

    public void loop3(Consumer<T> consumer) {
        Node<T> p = head;
        recursion(p,consumer);
    }

    /**
     * 递归处理链表元素
     *
     * @param consumer
     */
    private void recursion(Node<T> cur, Consumer<T> consumer) {
        if (cur == null) {
            return;
        }
        consumer.accept(cur.value);
        recursion(cur.next, consumer);
    }




    /**
     * 链表尾部增加节点
     *
     * @param value
     */
    public void addLast(T value) {
        Node<T> last = findLast();
        if (last == null) {
            addFirst(value);
            return;
        }
        last.next = new Node<>(value, null);
    }

    //找到链表最后一个节点
    public Node<T> findLast() {
        if (head == null) {
            return null;
        }
        Node<T> p = head;
        while (p.next != null) {
            p = p.next;
        }
        return p;
    }

    /**
     * 根据索引下标获取元素
     *
     * @param index
     * @return
     */
    public T get(int index) {
        Node<T> node = findNode(index);
        if (node == null) {
            throw getIllegalIndex(index);
        }
        return node.value;
    }

    private IllegalArgumentException getIllegalIndex(int index) {
        return new IllegalArgumentException(String.format("index [%d] 不合法", index));
    }

    private Node<T> findNode(int index) {
        if (index < 0) {
            return null;
        }
        int i = 0;
        for (Node<T> p = head; p != null; p = p.next, i++) {
            if (i == index) {
                return p;
            }
        }
        return null;
    }

    /**
     * 插入元素到指定索引处
     *
     * @param index
     * @param value
     */
    public void insert(int index, T value) {
        if (index == 0) {
            addFirst(value);
            return;
        }
        Node<T> prev = findNode(index - 1);
        if (prev == null) {
            throw getIllegalIndex(index);
        }
        prev.next = new Node<T>(value, prev.next);
    }

    /**
     * 删除链表第一个元素
     */
    public void removeFirst() {
        Node<T> node = findNode(0);
        if (node == null) {
            throw getIllegalIndex(0);
        }
        head = head.next;
    }

    /**
     * 删除指定索引处的节点
     *
     * @param index
     */
    public void remove(int index) {
        if (index == 0) {
            removeFirst();
            return;
        }
        Node<T> prev = findNode(index - 1);
        if (prev == null) {
            throw getIllegalIndex(index);
        }
        Node<T> node = prev.next;
        if (node == null) {
            throw getIllegalIndex(index);
        }
        prev.next = node.next;

    }

    public int length() {
        int i = 0;
        for (Node<T> p = head; p != null; p = p.next, i++) {
        }
        return i;
    }


    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private Node<T> p = head;

            @Override
            public boolean hasNext() {
                return p != null;
            }

            @Override
            public T next() {
                T value = p.value;
                p = p.next;
                return value;
            }
        };
    }


}
