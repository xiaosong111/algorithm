package com.qg.array;

import java.util.Arrays;

/**
 * @Author QG
 * @Date 2023/5/18 14:04
 * @description
 **/
public class Arraylist2<T> {
    private Object[] elementData;
    private int size = 0;
    private int initialCapacity = 10;

    public Arraylist2() {
        elementData = new Object[initialCapacity];
    }

    public void add(T o) {
        elementData[size++] = o;
    }

    public T get(int index) {
        return (T) elementData[size];
    }

    public <E> E[] toArray(E[] a) {
        return (E[]) Arrays.copyOf(elementData, size, a.getClass());
    }


    public static void main(String[] args) {
//        Arraylist2<String> list = new Arraylist2<>();
//        list.add("1");
//        list.add("2");
//        list.add("3");
//        String[] strs = new String[3];
//        String[] strArr = list.toArray(strs);
//        for (int i = 0; i < strArr.length; i++) {
//            System.out.println(strArr[i]);
//
//        }
    }
}
