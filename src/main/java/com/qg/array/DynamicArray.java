package com.qg.array;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.IntStream;

/**
 * @Author QG
 * @Date 2023/5/16 22:13
 * @description 自定义动态数组
 **/
public class DynamicArray implements RandomAccess, Iterable<Integer> {


    /**
     * 逻辑大小
     */
    private int size = 0;

    /**
     * 容量
     */
    private int capacity = 3;

    /**
     * 数组
     */
    private int[] array;


    /**
     * 添加元素到数组末尾
     *
     * @param element
     */
    public void addLast(int element) {
        add(size, element);
    }

    /**
     * 添加元素到索引为index的位置
     *
     * @param index
     * @param element
     */
    public void add(int index, int element) {
        if (index < 0) {
            throw new RuntimeException("索引不合法！");
        }
        checkAndGrow();
        if (index < size) {
            System.arraycopy(array, index, array, index + 1, size - index);
        }
        array[index] = element;
        size++;
    }

    /**
     * 检查并扩容
     */
    private void checkAndGrow() {
        if (size == 0) {
            array = new int[capacity];
        } else if (size == capacity) {
            capacity += (capacity >> 1);
            int[] newArray = new int[capacity];
            System.arraycopy(array, 0, newArray, 0, size);
            array = newArray;
        }
    }

    /**
     * 根据索引index获取单个元素
     *
     * @param index
     * @return
     */
    public int get(int index) {
        if (index < 0 && index >= size) {
            throw new RuntimeException("索引异常");
        }
        return array[index];
    }

    /**
     * 对数组元素依次执行consumer的accept()方法
     *
     * @param consumer
     */
    public void foreach(Consumer<Integer> consumer) {
        for (int i = 0; i < size; i++) {
            consumer.accept(array[i]);
        }
    }

    /**
     * 返回数组元素流
     *
     * @return
     */
    public IntStream stream() {
        return IntStream.of(Arrays.copyOfRange(array, 0, size));
    }


    /**
     * 实现迭代器
     *
     * @return
     */
    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            int i = 0;

            @Override
            public boolean hasNext() {
                return i < size;
            }

            @Override
            public Integer next() {
                return array[i++];
            }
        };
    }


    /**
     * 根据索引删除元素
     *
     * @param index
     * @return
     */
    public int remove(int index) {
        if (index < 0 || index >= size) {
            throw new RuntimeException("索引异常");
        }
        int remove = array[index];
        System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
        return remove;
    }


}
