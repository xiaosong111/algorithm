package com.qg.binarysearch;

/**
 * @Author QG
 * @Date 2023/5/11 20:31
 * @description 二分查找算法
 **/
public class BinarySearch {


    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4, 5, 6, 7, 8};
//        System.out.println(binarySearchBasic(a, 6));

        int[] b = new int[a.length + 1];
        System.arraycopy(a, 0, b, 0, a.length / 2);
        System.out.println(printStrByNums(b));
        b[a.length / 2] = -1;
        System.arraycopy(a, a.length / 2, b, a.length / 2 + 1, a.length - a.length / 2);
        System.out.println(printStrByNums(b));

    }

    public static String printStrByNums(int[] nums) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < nums.length; i++) {
            sb.append(nums[i]).append(" ");
        }
        return sb.toString();
    }


    /**
     * 二分查找基础版
     *
     * @param nums
     * @param target
     */
    public static int binarySearchBasic(int[] nums, int target) {
        int l = 0, r = nums.length - 1;
        while (l <= r) {
            int m = (l + r) >>> 1;
            if (target < nums[m]) {
                r = m - 1;
            } else if (target > nums[m]) {
                l = m + 1;
            } else {
                return m;
            }
        }
        return -1;
    }

    /**
     * 二分查找改进版
     *
     * @param nums
     * @param target
     * @return
     */
    public static int binarySearchAlter(int[] nums, int target) {
        int l = 0, r = nums.length;
        while (l < r) {
            int m = (l + r) >>> 1;
            if (target < nums[m]) {
                r = m;
            } else if (nums[m] < target) {
                l = m + 1;
            } else {
                return m;
            }
        }
        return -1;
    }


    /**
     * 平衡版二分查找，左右两边查找时间复杂度相等
     *
     * @param nums
     * @param target
     * @return
     */
    public int binarySearchBalance(int[] nums, int target) {
        int l = 0, r = nums.length, m = 0;
        while (1 < r - l) {
            m = (r + l) >>> 1;
            if (target < nums[m]) {
                r = m;
            } else {
                l = m;
            }
        }
        if (target == nums[m]) {
            return m;
        } else {
            return -1;
        }
    }

    /**
     * 遇到多个相同target值时返回最左边target值的下标
     *
     * @param nums
     * @param target
     * @return
     */
    public static int binarySearchLeftMost(int[] nums, int target) {
        int l = 0, r = nums.length - 1;
        int candidate = -1;
        while (l <= r) {
            int m = (l + r) >>> 1;
            if (target < nums[m]) {
                r = m - 1;
            } else if (target > nums[m]) {
                l = m + 1;
            } else {
                candidate = m;
                r = m - 1;
            }
        }
        return candidate;
    }


    /**
     * 遇到多个相同target值时返回最左边target值的下标
     * 如果target不存在，则返回target该插入的下标值
     *
     * @param nums
     * @param target
     * @return
     */
    public static int binarySearchLeftMost2(int[] nums, int target) {
        int l = 0, r = nums.length - 1;
        while (l <= r) {
            int m = (l + r) >>> 1;
            if (target <= nums[m]) {
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        return l;
    }


    /**
     * 遇到多个相同target值时返回最右边target值的下标
     * 如果target不存在，则返回target该插入的下标值
     *
     * @param nums
     * @param target
     * @return
     */
    public static int binarySearchRightMost2(int[] nums, int target) {
        int l = 0, r = nums.length - 1;
        int candidate = -1;
        while (l <= r) {
            int m = (l + r) >>> 1;
            if (target < nums[m]) {
                r = m - 1;
            } else if (target >= nums[m]) {
                l = m + 1;
            }
        }
        return l - 1;
    }


}
